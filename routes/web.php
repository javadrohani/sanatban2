<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','CategoryController@index');
Route::get('/category/create_category', 'CategoryController@create_category');
Route::post('/category/create_category', 'CategoryController@store');
Route::get('/category/delete/{id}','CategoryController@delete');
Route::get('/category/edit/{id}', 'CategoryController@show');
Route::PUT('/category/edit/{id}', 'CategoryController@edit');

Route::get('/attribute','AttributeController@index');
Route::get('/attribute/create_attribute', 'AttributeController@create_attribute');
Route::post('/attribute/create_attribute', 'AttributeController@store');
Route::get('/attribute/delete/{id}','AttributeController@delete');
Route::get('/attribute/edit_attribute/{id}', 'AttributeController@show');
Route::PUT('/attribute/edit_attribute/{id}','AttributeController@edit');

Route::get('/attribute_value','AttributeValueController@index');
Route::get('/attribute_value/create_attribute_value', 'AttributeValueController@create_attribute_value');
Route::post('/attribute_value/create_attribute_value', 'AttributeValueController@store');
Route::get('/attribute_value/delete/{id}','AttributeValueController@delete');
Route::get('/attribute_value/edit_attribute_value/{id}', 'AttributeValueController@show');
Route::PUT('/attribute_value/edit_attribute_value/{id}','AttributeValueController@edit');

Route::get('/product','ProductController@index');
Route::get('/product/create_product', 'ProductController@create_product');
Route::post('/product/create_product', 'ProductController@store');
Route::get('/product/delete/{id}','ProductController@delete');
Route::get('/product/edit_product/{id}', 'ProductController@show');
Route::PUT('/product/edit_product/{id}','ProductController@edit');

