@extends('welcome')
{{--@section('content')--}}
<a href="/attribute_value"><button type="button"  style="float: right; margin-top: 47px;" class="btn btn-primary"> برگشتن به عقب </button></a>
<br><br>

<h3 class="page-header" style="text-align: center;">ارسال مقادیر</h3>


<form action="" method="post" >
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-group">
        <label for="name" class="aligen">عنوان </label>
        <input class="form-control"  name="name"  value="{{--{{$attributes->name}}--}}" >
    </div>
    <div class="form-group">
        <label for="title" class="aligen"> مقادیر مشخصه</label>
        <div class="form-group">
            <label for="title" class="aligen"></label>
            <select name="attribute_id" class="form-control" id="attribute" title="دسته بندی خود را انتخاب کنید"  >
                {{--<option value="1">جاوا اسکریپت</option>
                     <option value="2">اندروید</option>
                     <option value="3">پی اچ پی</option>--}}
                @foreach( $categoriesAll as $id=>$name )

                    <option value="{{$id}}">{{$name}}</option>
                @endforeach
            </select>

        </div>

    <div class="form-group">
        <label for="title" class="aligen">وضعیت </label>
        <select name="status" id="status" title="وضعیت را انتخاب کنید" class="aligen">
            <option value="1">فعال</option>
            <option value="0">غیر فعال</option>
        </select>

    </div>

    <button type="submit" class="btn btn-primary">ویرایش مقادیر</button>

</form>
<br>
{{-- @endsection--}}

