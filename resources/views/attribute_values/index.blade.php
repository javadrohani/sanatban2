@extends('welcome')
{{--@section('content')
    @if($message = session('message'))--}}{{--پیام با موفقیت چاپ میشه--}}{{--
    <div class="alert alert-success" style="text-align: center">
        {{$message}}
    </div>
    @endif--}}

    <br>
    <h3  style="text-align: center;">جدول مشخصات</h3><br>

    <a  style="color: #c7254e" href="{{ URL::to('/attribute_value/create_attribute_value')}}">
        <button type="button" class="btn btn-primary">ایجاد مقادیر مشخصات</button>
    </a>

    <table  class="table tbl_box" style=" float: right; width:100%; text-align:center; padding:8px; border-color:#c7254e;">
        <br><br><br>

        <tr >
            <th  style ="text-align: center;"> مشخصه </th>
            <th  style ="text-align: center;">نام مقدار مشخصه </th>
            <th  style ="text-align: center;">وضعیت</th>
            <th  style ="text-align: center;">تاریخ ایجاد</th>
            <th  style ="text-align: center;">تاریخ ویرایش</th>
            <th  style ="text-align: center;">حذف</th>
            <th  style ="text-align: center;">ویرایش</th>


        </tr>

        <tr>
            @foreach( $attribute_values as $attribute_value )

                <td>{{$attribute_value->attribute->name}}</td>

                <td>{!! $attribute_value->name !!}</td>


                <td>{{$attribute_value->status==0 ? 'غیرفعال' :'فعال'}}</td>
                <td>{!! $attribute_value->created_at !!}</td>
                <td>{!! $attribute_value->updated_at !!}</td>
                <td>
                    <a  style="color: #c7254e" href="{{URL::to('/attribute_value/delete/'.$attribute_value->id)}}">

                        <i class="material-icons" style="color:red">delete</i>

                    </a>

                </td>

                <td>     <a  style="color: #c7254e" href="{{URL::to('/attribute_value/edit_attribute_value/'.$attribute_value->id)}}">

                        <i class="fa fa-edit" style="font-size:24px;color:#0000F0"></i>

                    </a>
                </td>
        </tr>

        @endforeach

    </table>
<br><br>
   {{-- <div style="text-align:center;">

        {!! $products->render() !!}
    </div>--}}
{{--
@endsection--}}
