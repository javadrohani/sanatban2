@extends('welcome')
{{--@section('content')--}}
    <a href="/"><button type="button"  style="float: right; margin-top: 47px;" class="btn btn-primary"> برگشتن به عقب </button></a>
<br><br>

    <h3 class="page-header" style="text-align: center;">ارسال دسته ها</h3>


<form action="" method="post" >
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-group">
        <label for="name" class="aligen">عنوان</label>
        <input class="form-control"  name="name"  value="{{$categories->name}}" >
    </div>

    <div class="form-group">
        <label for="title" class="aligen">دسته بندی</label>
        <select name="parent_id" id="parent_id" title=" انتخاب کنید" class="aligen">
            <option value="0">--سرگروه-- </option>
            @foreach( $categoriesAll as $id=>$name )
                <option value="{{$id}}">{{$name}}</option>
            @endforeach
        </select>
    </div>
    <br>
<br>
    <div class="form-group">
        <label for="title" class="aligen">وضعیت </label>
        <select name="status" id="status" title="وضعیت را انتخاب کنید" class="aligen">
            <option value="1">فعال</option>
            <option value="0">غیر فعال</option>
        </select>

    </div>

        <button type="submit" class="btn btn-primary">ویرایش دسته</button>

    </form>
    <br>
{{-- @endsection--}}

