@extends('welcome')
{{--@section('content')
    @if($message = session('message'))--}}{{--پیام با موفقیت چاپ میشه--}}{{--
    <div class="alert alert-success" style="text-align: center">
        {{$message}}
    </div>
    @endif--}}

    <br>
    <h3  style="text-align: center;">جدول مشخصات</h3><br>

    <a  style="color: #c7254e" href="{{ URL::to('/product/create_product')}}">
        <button type="button" class="btn btn-primary">ایجاد مقادیر مشخصات</button>
    </a>

    <table  class="table tbl_box" style=" float: right; width:100%; text-align:center; padding:8px; border-color:#c7254e;">
        <br><br><br>

        <tr >

            <th  style ="text-align: center;">نام محصول </th>
            <th  style ="text-align: center;">نام دسته بندی </th>
            <th  style ="text-align: center;">وضعیت</th>
            <th  style ="text-align: center;">تاریخ ایجاد</th>
            <th  style ="text-align: center;">تاریخ ویرایش</th>
            <th  style ="text-align: center;">حذف</th>
            <th  style ="text-align: center;">ویرایش</th>


        </tr>

        <tr>
            @foreach( $products as $product )

               <td>{!! $product->name !!}</td>
            {{--    @if(isset($category->parent->name))
                    <td>{{$category->parent->name}}</td>
                @else
                    <td>--</td>
                @endif--}}
            <td>{{$product->category->name}}</td>
{{--
                <td>{{$attribute_value->attribute->name}}</td>
--}}
                <td>{{$product->status==0 ? 'غیرفعال' :'فعال'}}</td>
                <td>{!! $product->created_at !!}</td>
                <td>{!! $product->updated_at !!}</td>
                <td>
                    <a  style="color: #c7254e" href="{{URL::to('/product/delete/'.$product->id)}}">

                        <i class="material-icons" style="color:red">delete</i>

                    </a>

                </td>

                <td>     <a  style="color: #c7254e" href="{{URL::to('/product/edit_product/'.$product->id)}}">

                        <i class="fa fa-edit" style="font-size:24px;color:#0000F0"></i>

                    </a>
                </td>
        </tr>

        @endforeach

    </table>
<br><br>
   {{-- <div style="text-align:center;">

        {!! $products->render() !!}
    </div>--}}
{{--
@endsection--}}
