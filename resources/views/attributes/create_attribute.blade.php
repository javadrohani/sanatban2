
@extends('welcome')


    <a href="/"><button type="button"  style="float: right; margin-top: 47px;" class="btn btn-primary"> برگشتن به عقب </button></a>

    <h3 class="page-header" style="text-align: center;">ایجاد دسته</h3>
<br><br>

        <form action=/attribute/create_attribute method="post" >
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="name" class="aligen">عنوان</label>
                <input class="form-control"  name="name">
            </div>
            <div class="form-group">
                <label for="title" class="aligen">مشخصه</label>
                <select name="category_id" class="form-control" id="category"   >
                    {{--<option value="1">جاوا اسکریپت</option>
                         <option value="2">اندروید</option>
                         <option value="3">پی اچ پی</option>--}}
                    @foreach( $categories as $id=>$name )

                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>

            </div>


            <div class="form-group">
                <label for="title" class="aligen">وضعیت </label>
                <select name="status" id="status" title="وضعیت را انتخاب کنید" class="aligen">
                    <option value="1">فعال</option>
                    <option value="0">غیر فعال</option>
                </select>

            </div>
            <button type="submit" class="btn btn-primary">ارسال مشخصات</button>

        </form>

    <br> <br> <br>

