<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
//use App\Category;


class Category extends Model
{
    use Sluggable;
    protected $fillable =[
        'parent_id',
        'name',
        'status',
        'slug',

    ];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ]
        ];
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

// recursive, loads all children
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

// load 1st level parent
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id', 'id');
    }

// recursive load all parents.
    public  function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public static  function createMenuTree($locations) { //return $locations;
        echo "<ul>";
        foreach ($locations as $location) {

            if (isset($location->children)) {//شرط چک میکنه اگه شرط برقرار است و بچه داریم /

                echo "<li>" . $location->name;//اول چاپ میکنه نام مادرشو//
                self::createMenuTree($location->children);//بچه های اون مادرو صدا میزنه//
                echo "</li>";
            }
            else {

                echo "<li>" . $location->name . "</li>";//اگه شرط برقرار نبودش نام مادر را فقط چاپ میکنه چون مادر هیچ بچه ای نداره//
            }
        }
        echo "</ul>";

        //return $str;
    }

    public function  product(){

        return $this->hasMany(Product::class/*,'attribute_id'*/);
    }
    public function  attribute(){

        return $this->hasMany(Attribute::class/*,'attribute_id'*/);
    }

}
