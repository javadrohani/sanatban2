<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Attribute;

class AttributeController extends Controller
{
    public function index ()
    {

        /*        $articles =Article::find(all)->get();*/
        $attributes = Attribute::select('*')/*->where('status', 1)*/->latest()->paginate(20);
        return view('attributes.index', compact('attributes'));

    }

    public function create_attribute ()
    {

        $categories = Category::all()->pluck('name', 'id');//اول بیا تمام مقدار کتگوری ها رو بگیر با استفاده از مدل کتگوری بعد همه رو برگردون و با استفاده از متد پلاک نام وآیدی رو میگیریم..//
        //return $categories;
        return view('attributes.create_attribute', compact('categories'));//کتگوری هارو میفرستیم به این ویو//
    }

    public function store(Request $request)
    {
        //return 'hxhx';

        $attributes = Attribute::create([
            //'user_id' => 1,
            'name' => $request['name'],
            'status' => $request['status'],
            'category_id' => $request['category_id'],

        ]);

       // return view('attributes.index', compact('attributes'));
        session()->flash('message','مشخصه با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/attribute');//کتگوری هارو میفرستیم به این ویو//*/
    }

    public function delete($id)
    {
        //return $id;
        Attribute::find($id)->delete();
        session()->flash('message','دسته با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //

        return back();

    }

    public function show($id){

        $attributes=Attribute::find($id);
        $categoriesAll = Category::all()->pluck('name', 'id'); //همه گروهها را میگیرد//
        //return 'gfg';
        return view('attributes.edit_attribute', compact('attributes','categoriesAll'));

    }
    public function edit(Request $request,$id){
       // return 'ss';
        Attribute::where('id', $id)
        ->update(['name'=> $request->name,'status'=>$request->status,'category_id'=>$request->category_id]);

        session()->flash('message','مشخصات با موفقییت ویرایش شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //

              return redirect('/attribute');


    }

}
