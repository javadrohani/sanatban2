<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;


class CategoryController extends Controller
{
    public function index ()
    {

        /*        $articles =Article::find(all)->get();*/
        $categories = Category::select('*')/*->where('status', 1)*/->latest()->paginate(20);
        // Article::find(17)->articles()->get()/*->where ('status',1)*/;//مقاله هایی که با یوزر آیدی 1 ثبت شده بر می گرداند/
        //$locations = Category::with('childrenRecursive')->whereNull('parent_id')->get();
        return view('categories.index', compact('categories'));
        //return 'ddhdh';

    }

    public function create_category ()
    {

        $categories = Category::all()->pluck('name', 'id');//اول بیا تمام مقدار کتگوری ها رو بگیر با استفاده از مدل کتگوری بعد همه رو برگردون و با استفاده از متد پلاک نام وآیدی رو میگیریم..//
        //return $categories;
        return view('categories.create_category', compact('categories'));//کتگوری هارو میفرستیم به این ویو//
    }

    public function store(Request $request)
    {
    //return 'hxhx';

       $categories = Category::create([
           //'user_id' => 1,
           'name' => $request['name'],
           'status' => $request['status'],
            'parent_id'=>$request['parent_id'],
        ]);

      //  $locations = Category::with('childrenRecursive')->whereNull('parent_id')->get();
        //return  $categories;
       // return redirect('/');//کتگوری هارو میفرستیم به این ویو//*/
        //return view('categories.index', compact('categories'));
        session()->flash('message','دسته با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
          return redirect('/');//کتگوری هارو میفرستیم به این ویو//*/
        // return back();

    }



    public function delete($id)
    {
        //return $id;
        Category::find($id)->delete();
        session()->flash('message','دسته با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //

        return back();

    }

    public function show($id){

        $categories=Category::find($id);//فقط گروهی که id گرفته//

        $categoriesAll = Category::all()->pluck('name', 'id'); //همه گروهها را میگیرد//
        //return 'gfg';
        return view('categories.edit', compact('categories','categoriesAll'));

    }
    public function edit(Request $request,$id){
        Category::where('id', $id)
            ->update(['name'=> $request->name,'status'=>$request->status,'parent_id'=>$request->parent_id]);

        session()->flash('message','دسته با موفقییت ویرایش شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/');
         //return back();
    }
}
