<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index ()
    {

        $products = Product::select('*')/*->where('status', 1)*/->latest()->paginate(20);
        // Article::find(17)->articles()->get()/*->where ('status',1)*/;//مقاله هایی که با یوزر آیدی 1 ثبت شده بر می گرداند/
        //$locations = Category::with('childrenRecursive')->whereNull('parent_id')->get();
        return view('products.index', compact('products'));
        //return 'ddhdh';

    }
    public function create_product ()
    {
        //zeynab jan when want to show per view have to connect to view table in data base

        $categories = Category::all()->pluck('name', 'id');//اول بیا تمام مقدار کتگوری ها رو بگیر با استفاده از مدل کتگوری بعد همه رو برگردون و با استفاده از متد پلاک نام وآیدی رو میگیریم..//
        //return   $attribute_values;
        return view('products.create_product', compact('categories'));//کتگوری هارو میفرستیم به این ویو//
    }


    public function store(Request $request)
    {
        //  dd($request->all());
       // return $request['category_id'];
        $products= Product::create([//'user_id' => 1,
            'name' => $request['name'], 'status' => $request['status'],
            //'attribute_id' => $attribute_values->id,
            //'attribute_id'=>$request = $id,
            //'attribute_id'=>$user->id,
            //'attribute_id' =>1,
            'category_id' => $request['category_id'],]);

        session()->flash('message',' محصول با موفقییت ثبت شد ');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/product');//کتگوری هارو میفرستیم به این ویو//*/
    }

    public function delete($id)
    {
        //return $id;
        Product::find($id)->delete();
        session()->flash('message','محصول با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/product');//کتگوری هارو میفرستیم به این ویو//*/

    }

    public function show($id)
    {

        $products = Product::find($id);
        $categoriesAll = Category::all()->pluck('name', 'id'); //همه گروهها را میگیرد//
        //return 'gfg';
        return view('products.edit_product', compact('products','categoriesAll'));
    }

    public function edit(Request $request,$id){
        Product::where('id', $id)
            ->update(['name'=> $request->name,'status'=>$request->status,'category_id'=>$request->category_id]);
        session()->flash('message','محصول با موفقییت ویرایش شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        //return redirect('/category');//کتگوری هارو میفرستیم به این ویو//*/
        return redirect('/product');
    }



}
