<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Attribute_value;
use Illuminate\Http\Request;

class AttributeValueController extends Controller
{
    public function index ()
    {

        /*        $articles =Article::find(all)->get();*/
        $attribute_values = Attribute_value::select('*')/*->where('status', 1)*/->latest()->paginate(20);
        // Article::find(17)->articles()->get()/*->where ('status',1)*/;//مقاله هایی که با یوزر آیدی 1 ثبت شده بر می گرداند/
        //$locations = Category::with('childrenRecursive')->whereNull('parent_id')->get();
        return view('attribute_values.index', compact('attribute_values'));
        //return 'ddhdh';

    }
    public function create_attribute_value ()
    {
        //zeynab jan when want to show per view have to connect to view table in data base

        $attribute = Attribute::all()->pluck('name', 'id');//اول بیا تمام مقدار کتگوری ها رو بگیر با استفاده از مدل کتگوری بعد همه رو برگردون و با استفاده از متد پلاک نام وآیدی رو میگیریم..//
        //return   $attribute_values;
        return view('attribute_values.create_attribute_value', compact('attribute'));//کتگوری هارو میفرستیم به این ویو//
    }

    public function store(Request $request)
    {
      //  dd($request->all());
       // return $request['attribute_id'];
       $attribute_values = Attribute_value::create([
            //'user_id' => 1,
            'name' => $request['name'],
            'status' => $request['status'],
            //'attribute_id' => $request[''],
            //'attribute_id' => $attribute_values->id,
            //'attribute_id'=>$request = $id,
            //'attribute_id'=>$user->id,
            //'attribute_id' =>1,
            'attribute_id' =>$request['attribute_id'],
        ]);

        // return $attribute_values;
       //$attribute_values->attribute()->attach(request('attribute'));//تمام مقدار categoryدر قسمت سلکت که nameبرابر با category هست میزاریم...وcategoriesاون رابطه ای که در مدل آرتیک ایجاد کردیم هست//


        // return view('attributes.index', compact('attributes'));
        session()->flash('message','مشخصه با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/attribute_value');//کتگوری هارو میفرستیم به این ویو//*/
    }
    public function delete($id)
    {
        //return $id;
        Attribute_value::find($id)->delete();
        session()->flash('message','مقاله با موفقییت حذف شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        return redirect('/attribute_value');//کتگوری هارو میفرستیم به این ویو//*/

    }

    public function show($id)
    {

        $attributes= Attribute::find($id);
        $categoriesAll = Attribute_value::all()->pluck('name', 'id'); //همه گروهها را میگیرد//


        //return 'gfg';
        return view('attribute_values.edit_attribute_value', compact('attributes','categoriesAll'));
    }

    public function edit(Request $request,$id){
        Attribute_value::where('id', $id)
            ->update(['name'=> $request->name,'status'=>$request->status,'attribute_id'=>$request->attribute_id]);
        session()->flash('message','مقادیر با موفقییت ویرایش شد');//این سشن برای گرفتن اینکه پیام  ادد بشه مقاله //
        //return redirect('/category');//کتگوری هارو میفرستیم به این ویو//*/
        return redirect('/attribute_value');
    }

}
