<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable =[
        'name',
        'status',
        'category_id',
    ];


    public function  attribute_value(){

        return $this->hasMany(Attribute_value::class/*,'attribute_id'*/);
    }

    public function  category(){

        return $this->belongsTo(Category::class/*,'attribute_id'*/);
    }
}

