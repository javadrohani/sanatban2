<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute_value extends Model
{
    protected $fillable =[
        'name',
        'status',
        'attribute_id'

    ];
    public  function attribute(){

        return $this->belongsTo(Attribute::class);//نام کلاسی که با آن در ارتباط است//
    }
}
